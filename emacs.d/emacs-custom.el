;;; emacs-custom.el --- DO NOT EDIT

;;; Commentary:
;; File generated automativallyby Emacs
;; Do not modified!

;;; Code:
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(custom-enabled-themes (quote (atom-one-dark)))
 '(custom-safe-themes
   (quote
	("2642a1b7f53b9bb34c7f1e032d2098c852811ec2881eec2dc8cc07be004e45a0" "669e02142a56f63861288cc585bee81643ded48a19e36bfdf02b66d745bcc626" "a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" default)))
 '(delete-selection-mode nil)
 '(fci-rule-color "#37474f")
 '(font-use-system-font t)
 '(global-display-line-numbers-mode t)
 '(highlight-indent-guides-responsive (quote stack))
 '(hl-sexp-background-color "#1c1f26")
 '(indicate-empty-lines t)
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
	(blacken ## auctex-latexmk highlight-indent-guides django-mode virtualenv virtualenvwrapper atom-dark-theme atom-one-dark-theme color-theme magit elpy auctex flycheck-package flycheck py-autopep8 yasnippet-snippets)))
 '(py-autopep8-options nil)
 '(scroll-bar-mode nil)
 '(server-switch-hook
   (quote
	((lambda nil
	   (let
		   (server-buf)
		 (setq backup-by-copying t))))))
 '(show-paren-mode t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
	((20 . "#f36c60")
	 (40 . "#ff9800")
	 (60 . "#fff59d")
	 (80 . "#8bc34a")
	 (100 . "#81d4fa")
	 (120 . "#4dd0e1")
	 (140 . "#b39ddb")
	 (160 . "#f36c60")
	 (180 . "#ff9800")
	 (200 . "#fff59d")
	 (220 . "#8bc34a")
	 (240 . "#81d4fa")
	 (260 . "#4dd0e1")
	 (280 . "#b39ddb")
	 (300 . "#f36c60")
	 (320 . "#ff9800")
	 (340 . "#fff59d")
	 (360 . "#8bc34a"))))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(flycheck-error ((t (:underline "#E06C75"))))
 '(flycheck-info ((t (:underline "#98C379"))))
 '(flycheck-warning ((t (:underline "#D19A66"))))
 '(highlight-indent-guides-character-face ((t (:foreground "dim gray" :weight ultra-light))))
 '(highlight-indent-guides-stack-character-face ((t (:foreground "dim gray"))))
 '(highlight-indentation-current-column-face ((t nil)))
 '(highlight-indentation-face ((t nil)))
 '(powerline-active1 ((t (:background "#2C323C" :foreground "DeepSkyBlue3"))))
 '(powerline-active2 ((t (:background "#2C323C" :foreground "steel blue"))))
 '(whitespace-hspace ((t (:background "grey24" :foreground "darkgray"))))
 '(whitespace-indentation ((t (:background "yellow" :foreground "firebrick"))))
 '(whitespace-newline ((t (:foreground "dim gray" :weight ultra-light))))
 '(whitespace-space ((t (:foreground "dimgray" :weight ultra-light :width ultra-condensed))))
 '(whitespace-tab ((t (:background "grey22" :foreground "yellow")))))

(provide 'emacs-custom)
;;; emacs-custom.el ends here
