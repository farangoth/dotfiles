;;;  init.el -- emacs configuration

;;; Commentary:
;;

;;; Code:

;; Use emacs as a server
;; Link with 'emacsclient -nc'
(require 'server)
(unless(server-running-p)
  (server-start))

(add-to-list 'exec-path "/home/farangoth/.local/bin/")

; ; Package management
(require 'package)
(add-to-list 'package-archives
	     '("melpa-stable" . "https://melpa.org/packages/") t)
(package-initialize)
(when(not package-archive-contents)
  (package-refresh-contents))


(defvar package-myPackages
  '(magit
    powerline
    highlight-indent-guides
	diminish
    yasnippet-snippets
    django-snippets
    django-mode
    py-autopep8
    flycheck
    flycheck-package
    auctex
    elpy
    virtualenv
    virtualenvwrapper
    ))

(mapc  #'(lambda (package)
	  (unless(package-installed-p package)
	    (package-install package)))
	   package-myPackages)


(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
(setq highlight-indent-guides-method 'character)
(fset 'yes-or-no-p 'y-or-n-p)
(setq confirm-nonexistent-file-or-buffer nil)
(setq kill-buffer-query-functions
  (remq 'process-kill-buffer-query-function
         kill-buffer-query-functions))

;; Powerline
(require 'powerline)
(powerline-center-theme)

;; Snippet:
(add-to-list 'load-path
	     (concat user-emacs-directory "/snippets/"))
(require 'yasnippet)
(yas-global-mode 1)

;; Raccourcis clavier
(global-set-key (kbd "C-x &") 'delete-other-windows)
(global-set-key (kbd "C-x é") 'split-window-below)
(global-set-key (kbd "C-x \"") 'split-window-right)
(global-set-key (kbd "C-x à") 'delete-window)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)

;; systemd
(add-to-list 'auto-mode-alist '("\\.service\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.rules\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.timer\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.target\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.mount\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.automount\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.slice\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.socket\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.path\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.netdev\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.network\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.link\\'" . conf-unix-mode))
(add-to-list 'auto-mode-alist '("\\.automount\\'" . conf-unix-mode))

;; AucTeX
(setq TeX-PDF-mode t)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

(require 'auctex-latexmk)
(auctex-latexmk-setup)

;; Python
(require 'elpy)
(elpy-enable)

(require 'py-autopep8)
(setq py-autopep8-options '("--ignore=E402"))
(add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
(add-hook 'default-tab-width 4)

(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))
(global-flycheck-mode)

(setq-local flycheck-python-flake8-executable "python3")
(setq-local flycheck-python-pylint-executable "python3")

;; Use IPython for REPL
(setq python-shell-interpreter "ipython3")
(setq python-shell-interpreter-args "-i --simple-prompt")

(require 'virtualenv)
(require 'virtualenvwrapper)
; (venv-initialize-interactive-shells) ;; if you want interactive shell support
; (venv-initialize-eshell) ;; if you want eshell support
;; note that setting `venv-location` is not necessary if you
;; use the default location (`~/.virtualenvs`), or if the
;; the environment variable `WORKON_HOME` points to the right place
(setq venv-location "~/.virtualenvs/")

;; Django
(require 'django-html-mode)
(require 'django-mode)
(add-to-list 'auto-mode-alist '("\\.html$" . django-html-mode))

(eval-after-load 'flycheck
   '(progn
      (flycheck-add-mode 'python-pylint 'django-mode)
      (flycheck-add-mode 'python-flake8 'django-mode)))

; (setq elpy-parentheses-after-callables t)

;; Backups files dans un meme dossier
 (setq auto-save-file-name-transforms
	   `((".*" ,temporary-file-directory t)))

;; customize -- separated file
(setq custom-file (locate-user-emacs-file "emacs-custom.el"))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))
(load custom-file)

;; Write backups to ~/.emacs.d/backup/
(setq backup-directory-alist '(("." . "~/.emacs.d/backups/"))
	 ;backup-by-copying      t  ; Don't de-link hard links
      version-control        t  ; Use version numbers on backups
      delete-old-versions    t  ; Automatically delete excess backups:
      kept-new-versions      20 ; how many of the newest versions to keep
      kept-old-versions      5 ; and how many of the old
)

(setq show-trailing-whitespace t)
(electric-pair-mode t)
(setq visible-bell 1)

(custom-set-variables
 '(server-switch-hook
   '((lambda nil
	   (let (server-buf)
		 (setq backup-by-copying t)
		 ))))
 )

(require 'diminish)
(diminish 'auto-revert)
(diminish 'highlight-indent-guides-mode)


(provide 'init)
;;; init.el ends here
